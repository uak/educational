#!/bin/bash

echo
echo 'Send "Version"+"Previous block hash"+"Merkle root"+"Time stamp"+"Difficulty bits"+"Nonce(Random)"'
echo "to sha256sum"
sleep 3
while :
do
      # Generate random number
      checksum=$(dd if=/dev/urandom bs=1 count=128 status=none | sha256sum )
      # If hash starts with two zeros then notify that a block is found
      if [[ $checksum == 00* ]]
      then 
	    # Colour the leading two zeros
	    echo "$checksum" | ack --passthru ^00
	    # tupt is used to colour the Block Found text
	    tput setab 1; echo -n "BLOCK FOUND"
	    # Reset color to default
	    tput sgr0 ; echo
	    echo
	    break
      else
	      echo "$checksum"
      fi
    sleep 0.1
done

